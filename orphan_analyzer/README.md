# Orphan Analyzer
## Overview
This module is designed to analyse the orphan word and return a response with relevant top 5 ontology classes. Its build using Python's micro framework named as [Flask](http://flask.pocoo.org/) and [Flask Restful](https://flask-restful.readthedocs.io/en/latest/).
There are several endpoints as mentioned below.

### `/paradigmatic`
This endpoint is intended to calculate paradigmatic similarity score between two words.

Example:

```
curl http://localhost:5000/paradigmatic?word1=angular&word2=jquery

Output:
{'score': 0.231341412}
```

### `/allocation`
This endpoint is intended to calculate the paradigmatic similarity score between the orphan word and combination word from ontology data. Perform analysis agaist each ontology class then compute each classes along with their labels and return top 5 max scored classes.

Example:
```
curl http://localhost:5000/allocation?orphan=kafka

Output
{ 'result': [
    {
      "node": "Messaging",
      "instances": [
        {
          "instance": "WCF",
          "occurrence": 4058,
          "combination": "kafka, WCF",
          "score": 0.07093174755573273
        },
        {
          "instance": "wcf services",
          "occurrence": 484,
          "combination": "kafka, wcf services",
          "score": 0.07601606845855713
        },
        {
          "instance": "w.c.f",
          "occurrence": 1,
          "combination": "kafka, w.c.f",
          "score": 0
        },
        {
          "instance": "JMS",
          "occurrence": 1884,
          "combination": "kafka, JMS",
          "score": 0.1027916967868805
        },
        {
          "instance": "Rabbit_Mq",
          "occurrence": 62,
          "combination": "kafka, Rabbit_Mq",
          "score": 0.0866813212633133
        },
        {
          "instance": "mq",
          "occurrence": 1178,
          "combination": "kafka, mq",
          "score": 0.1070961281657219
        },
        {
          "instance": "rabbit-mq",
          "occurrence": 62,
          "combination": "kafka, rabbit-mq",
          "score": 0
        },
        {
          "instance": "rabbitmq",
          "occurrence": 202,
          "combination": "kafka, rabbitmq",
          "score": 0.17202557623386383
        }
      ],
      "maxScore": 0.17202557623386383,
      "avgScore": 0.08631635457277298
    },
    {
      "node": "Scheduling",
      "instances": [
        {
          "instance": "CRON",
          "occurrence": 513,
          "combination": "kafka, CRON",
          "score": 0.10201465338468552
        },
        {
          "instance": "Autosys",
          "occurrence": 510,
          "combination": "kafka, Autosys",
          "score": 0.05800338089466095
        },
        {
          "instance": "autosys triggers",
          "occurrence": 0,
          "combination": "kafka, autosys triggers",
          "score": 0
        },
        {
          "instance": "Oozie",
          "occurrence": 379,
          "combination": "kafka, Oozie",
          "score": 0.16559813916683197
        }
      ],
      "maxScore": 0.16559813916683197,
      "avgScore": 0.1031932532787323
    },
    {
      "node": "Data_Streaming",
      "instances": [
        {
          "instance": "APEX",
          "occurrence": 547,
          "combination": "kafka, APEX",
          "score": 0.07495053112506866
        },
        {
          "instance": "Storm",
          "occurrence": 308,
          "combination": "kafka, Storm",
          "score": 0.16208791732788086
        },
        {
          "instance": "apache storm topology",
          "occurrence": 2,
          "combination": "kafka, apache storm topology",
          "score": 0.0037878789007663727
        },
        {
          "instance": "Gemfire",
          "occurrence": 27,
          "combination": "kafka, Gemfire",
          "score": 0.05532681569457054
        }
      ],
      "maxScore": 0.16208791732788086,
      "avgScore": 0.10455024987459183
    },
    {
      "node": "Hadoop_Ecosystem",
      "instances": [
        {
          "instance": "HDFS",
          "occurrence": 1125,
          "combination": "kafka, HDFS",
          "score": 0.1523081660270691
        },
        {
          "instance": "webhdfs",
          "occurrence": 0,
          "combination": "kafka, webhdfs",
          "score": 0
        },
        {
          "instance": "Hive",
          "occurrence": 2156,
          "combination": "kafka, Hive",
          "score": 0.12528544664382935
        },
        {
          "instance": "Map-Reduce",
          "occurrence": 389,
          "combination": "kafka, Map-Reduce",
          "score": 0
        },
        {
          "instance": "mapreduce",
          "occurrence": 894,
          "combination": "kafka, mapreduce",
          "score": 0.1460375189781189
        }
      ],
      "maxScore": 0.1523081660270691,
      "avgScore": 0.12533296644687653
    },
    {
      "node": "nosql",
      "instances": [
        {
          "instance": "Hadoop",
          "occurrence": 3598,
          "combination": "kafka, Hadoop",
          "score": 0.09971044957637787
        },
        {
          "instance": "MongoDB",
          "occurrence": 2740,
          "combination": "kafka, MongoDB",
          "score": 0.11927692592144012
        },
        {
          "instance": "mongo",
          "occurrence": 776,
          "combination": "kafka, mongo",
          "score": 0.15093784034252167
        },
        {
          "instance": "mangodb",
          "occurrence": 24,
          "combination": "kafka, mangodb",
          "score": 0.045396242290735245
        },
        {
          "instance": "Pig",
          "occurrence": 1291,
          "combination": "kafka, Pig",
          "score": 0.14956891536712646
        },
        {
          "instance": "piglatin",
          "occurrence": 4,
          "combination": "kafka, piglatin",
          "score": 0.009363295510411263
        }
      ],
      "maxScore": 0.15093784034252167,
      "avgScore": 0.11821714043617249
    }
  ]
}
  ```
There some complexity involve while performing the analysis, we are making a mapping structure with orphan word and all entire ontology classes as mentioned in the above response data.

### `/reset-occurrences`
This endpoint is intended to perform an asynchronous operation for setting up the occurrences count for each node in the ontology database.

Occurrence count would be summation of all labels and instance name occurrences.

It doesn't response any useful data instead returns a successful acknowledge message.

Example:
```
curl http://localhost:5000/reset-occurrences

Output:
{'message': 'nodes occurrence update started...'}

```
## Setup
In order to start working on this module. Its required to enable the virtual environment and install all dependencies packages.

Developers need to follow below mentioned steps to enable to development environment.

1. `cd orphan_analyzer/`
2. Enable the virtual environment using `pipenv shell` command.
3. Install all dependencies using `pipenv install` command. This command should run once only if developer is trying to setup the environment for the first time.
4. Now everything is setup, You can server the endpoint resources by executing the command `python resources.py`. It would start the localhost server at port 5000.

## Deployment
We configure this microservice with [Zappa](https://github.com/Miserlou/Zappa). Zappa makes it super easy to build and deploy server-less, event-driven Python applications (including, but not limited to, WSGI web apps) on AWS Lambda + API Gateway. Think of it as "serverless" web hosting for your Python apps. That means infinite scaling, zero downtime, zero maintenance - and at a fraction of the cost of your current deployments!

[Zappa Documentation](https://github.com/Miserlou/Zappa)

Once you configured the zappa and its prerequisites then you can easily deploy or update the deployment as mentioned in below commands.

- For newly/first deployment, run `zappa deploy dev` command. It would executes the new deployment based on zappa_settings.json file.
- For updating existing deployment, run `zappa update dev` command. It would update the existing deployment.
