import os

class Config(object):
    DEBUG = False
    NEO4J_URI = os.environ.get("bolt://3.16.30.216:7687/")
    NEO4J_USERNAME = os.environ.get('neo4j')
    NEO4J_PASSWORD = os.environ.get('neo4j')

class ProductionConfig(Config):
    DEBUG = False

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True

config = {
    'dev': DevelopmentConfig,
    'prod': ProductionConfig,
    'test': TestingConfig
}