import os
import re
import copy

from neo4j.v1 import GraphDatabase
from concurrent.futures import ThreadPoolExecutor
from itertools import repeat
from elasticsearch import Elasticsearch

from app import app
from es import scroll

import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Neo4JBase(object):
    print("Reached here 1")
    try:
        driver = GraphDatabase.driver(
        app.config['NEO4J_URI'],
        auth=(
            app.config['NEO4J_USERNAME'],
            app.config['NEO4J_PASSWORD']
            )
        )    
    except Exception as e:
        print(e)    
    print("Reached here 2")
    es = Elasticsearch(hosts=os.environ.get('ELASTICSEARCH_HOST'))
    print("Reached here 3")
    
    def query(self, chiper_query):
        print("in query")
        data = ''
        with self.driver.session() as session:
            with session.begin_transaction() as tx:
                data = tx.run(chiper_query)
        return data
    print("Reached here 4")
    def get_nodes_with_labels(self, response):
        print("in get_nodes_with_labels")
        for node_name, node_labels in response.values():
            yield node_name, node_labels
    print("Reached here 5")
    def get_node_occurrences(self, node_name):
        try:
            print("in get_node_occurrences")
            node_name = node_name.replace('_', ' ')
            node_name = node_name.lower()
            count = 0
            body = {
                    "query": {
                        "match_phrase": {
                                "text": node_name
                            }
                    },
                    "_source": "{}",
                    "highlight": {
                        "fields": {
                        "text": {
                            "fragment_size": 200,
                            "number_of_fragments": 0
                        }
                        }
                    }
                }

            for total_pages, page_counter, page_items, page_data in scroll(index=os.environ.get('CORPUS_NAME'), doc_type=os.environ.get('DOC_TYPE'), query_body=body):
                for page_data_hits in page_data['hits']['hits']:
                    for line in page_data_hits['highlight']['text']:
                        try:
                            line = line.replace('<em>', '').replace('</em>', '')
                            node_name = node_name.replace('-', ' ')
                            node_name = re.sub('[^a-zA-Z0-9\ ]+', '', node_name)
                            count += sum(1 for match in re.finditer(r"\b%s\b" % node_name, line))
                        except Exception as e:
                            print(node_name,e)
            return count
        except Exception as e:
            print (node_name, e)
            return count

    def set_node_occurrence(self, node_name, labels):
        print("in set_node_occurrence")
        logger.info('proccessing')
        logger.info(node_name)
        node_count = 0
        node_with_labels = []
        node_with_labels.append(node_name)
        node_with_labels.extend(labels)
        node_with_labels = list(set(node_with_labels))
        for label in node_with_labels:
            node_count += self.get_node_occurrences(label)
        logger.info('setting node_count')
        logger.info(node_name)
        logger.info(node_count)
        self.query(
            'MATCH (n { name:"%s" }) \
            SET n.occurrence = %s \
            RETURN n.name, n.occurrence' % (
            node_name,
            node_count
        ))


    def reset_node_occurrences(self):
        print("in reset_node_occurrences")
        response = self.query(chiper_query='MATCH (n) RETURN (n.name), labels(n)')
        with ThreadPoolExecutor(50) as pool:
            for node_name, labels in self.get_nodes_with_labels(response):
                pool.submit(self.set_node_occurrence, node_name, labels)