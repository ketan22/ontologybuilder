import logging
import json
import boto3
import uuid

from multiprocessing.pool import ThreadPool
from flask_restful import Api as FlaskRestfulAPI, Resource,reqparse
from zappa.async import task

from ontology import Neo4JBase
from utils import get_ontology_mapping_with_orphan, get_computed_nodes, calculate_paradigmatic
from app import app


logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)



@task
def reset_node_occurrences(neo4j):
    neo4j.reset_node_occurrences()


class OntologyResource(Resource):

    def __init__(self, *args, **kwargs):
        self.neo4j = Neo4JBase()

    def get(self):
        try:
            reset_node_occurrences(self.neo4j)
            return {'message': 'nodes occurrence update started...'}
        except Exception as e:
            logger.error(e)
            return {'error': str(e)}


class OrphanAnalyzerResource(Resource):
    get_parser = reqparse.RequestParser()
    get_parser.add_argument('orphan', type=str, required=True)

    def get(self):
        try:
            args = self.get_parser.parse_args()
            ontology_mapping = get_ontology_mapping_with_orphan(args['orphan'])
            topic_id = str(uuid.uuid4())
            logger.info('Topic ID {}'.format(topic_id))
            pool = ThreadPool(processes=1)
            async_result = pool.apply_async(get_computed_nodes, (topic_id, len(ontology_mapping)))
            calculate_paradigmatic(topic_id, ontology_mapping)
            computed_nodes = async_result.get()
            # max_scored_computed_nodes, avg_scored_computed_nodes = async_result.get()
            # return {'max_scored_result': max_scored_computed_nodes[:5], 'avg_scored_result': avg_scored_computed_nodes[:5]}
            return {'result': computed_nodes[:5]}
        except Exception as e:
            logger.error(e)
            return {'error': str(e)}


class ParadigmaticResource(Resource):
    get_parser = reqparse.RequestParser()
    get_parser.add_argument('word1', type=str)
    get_parser.add_argument('word2', type=str)

    def get(self):
        try:
            args = self.get_parser.parse_args()
            data = {'path': 'paradigmatic', 'word1': args['word1'], 'word2':args['word2']}
            client = boto3.client('lambda')
            response = client.invoke(FunctionName='paradigmatic-analyzer-dev',InvocationType='RequestResponse', Payload=json.dumps(data).encode('utf-8'))
            print(response)
            return json.loads(response['Payload'].read())
        except expression as identifier:
            logger.error(e)
            return {'error': str(e)}


with app.app_context():
    api = FlaskRestfulAPI(app)
    api.add_resource(OrphanAnalyzerResource, '/allocation')
    api.add_resource(OntologyResource, '/reset-occurrences')
    api.add_resource(ParadigmaticResource, '/paradigmatic')


if __name__ == '__main__':
    app.run(debug=True)
