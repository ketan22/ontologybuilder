import requests
import csv
import json
import time
import copy
from collections import Counter
from ontology import Neo4JBase


def write_into_file(filename, data):
    with open('{}.csv'.format(filename), 'w') as file:
        writer = csv.writer(file)
        writer.writerows(data)
        print("Writing complete")

def get_marks(node, data):
    marks = 0 if node not in data else 5 - data.index(node)
    return marks

def test_paradigmatic_analysis():
    neo4j = Neo4JBase()
    response = neo4j.query(
        "MATCH (n)-[r:ParentOf*1..1]->(m) \
        WHERE n.Type ='OCLASS' AND m.Type='OINSTANCE' AND not (m.occurrence=null and m.occurrence=0) \
        WITH n, m ORDER BY m.occurrence DESC \
        RETURN n.name, COLLECT([m.name, m.occurrence, labels(m)])[..4] as instances order by instances[0][1] desc")
    nodes = response.values()
    max_score_data = [
        [
            '4th Instance', 'Occurrences', 'Original Parent Class',
            'Suggested 1st Rank Class', 'Suggested 2nd Rank Class',
            'Suggested 3rd Rank Class', 'Suggested 4th Rank Class',
            'Suggested 5th Rank Class, Marks, Execution Time'
        ]
    ]
    avg_score_data = copy.deepcopy(max_score_data)
    max_obtained_marks = []
    avg_obtained_marks = []
    for node, instances in nodes:
        instances = instances[-1:]
        if instances:
            start_time = time.time()
            instance_name = instances[0][0]
            occurrence = instances[0][1]
            instances[0][2].append(instance_name)
            labels = list(map(lambda x: x.replace('_', ' ').lower(), instances[0][2]))
            labels = list(set(labels))
            for label in labels:
                max_raw_data = [label, occurrence, node]
                avg_raw_data = copy.deepcopy(max_raw_data)
                response = requests.get('http://localhost:5000/allocation?orphan={}'.format(label))
                max_ranked_classes = [i['node'] for i in response.json()['max_scored_result']]
                avg_ranked_classes = [i['node'] for i in response.json()['avg_scored_result']]
                print("Execution time {}".format(time.time() - start_time))

                # calculate marks and append into last column
                max_obtained_marks.append(get_marks(node, max_ranked_classes))
                max_ranked_classes.append(get_marks(node, max_ranked_classes))
                max_raw_data.extend(max_ranked_classes)

                avg_obtained_marks.append(get_marks(node, avg_ranked_classes))
                avg_ranked_classes.append(get_marks(node, avg_ranked_classes))
                avg_raw_data.extend(avg_ranked_classes)

                print(max_raw_data)
                print (avg_raw_data)
                max_score_data.append(max_raw_data)
                avg_score_data.append(avg_raw_data)
    max_score_data.append(['', '', '', '', '', '', '', 'Total Obtained marks', sum(max_obtained_marks)])
    max_score_data.append(['', '', '', '', '', '', '', 'Stats', json.dumps(dict(Counter(max_obtained_marks)))])
    write_into_file('test_result_data_Max_Score_with_2_words', max_score_data)

    avg_score_data.append(['', '', '', '', '', '', '', 'Total Obtained marks', sum(avg_obtained_marks)])
    avg_score_data.append(['', '', '', '', '', '', '', 'Stats', json.dumps(dict(Counter(avg_obtained_marks)))])
    write_into_file('test_result_data_Avg_Score_with_2_words', avg_score_data)


if __name__ == '__main__':
    test_paradigmatic_analysis()