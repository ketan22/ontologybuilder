import os
import uuid
import confluent_kafka

from ontology import Neo4JBase
from node_pb2 import NodeClass
from google.protobuf.json_format import MessageToJson
from concurrent.futures import ThreadPoolExecutor
from itertools import repeat
from operator import itemgetter


conf = {
        'bootstrap.servers': "18.216.132.48:9092",
        'session.timeout.ms': 6000,
        'group.id': uuid.uuid1(),
        'default.topic.config': {
            'auto.offset.reset': 'earliest',
        }
}

# conf = {'bootstrap.servers': os.environ.get('KAFKA_BROKER_SERVERS'),
#         'session.timeout.ms': 6000,
#         'group.id': uuid.uuid1(),
#         'default.topic.config': {
#             'auto.offset.reset': 'earliest',
#         }
# }

consumer = confluent_kafka.Consumer(**conf)


def get_ontology_mapping_with_orphan(orphan_word):
    neo4j = Neo4JBase()
    ontology_mapping = list()
    data = neo4j.query(
        "MATCH (n)-[r:ParentOf*1..1]->(m) \
        WHERE n.Type ='OCLASS' AND m.Type='OINSTANCE' AND not (m.occurrence=null and m.occurrence=0) \
        WITH n, m ORDER BY m.occurrence DESC \
        RETURN n.name, COLLECT([m.name, m.occurrence, labels(m)])[..3] as instances order by instances[0][1] desc")


    data = neo4j.query("MATCH (n)-[r:PARENT_OF*1..1]->(m) WHERE n.Type ='OCLASS' and m.type='OINSTANCE'   return m,n")

    nodes = data.values()
    for node, instances in nodes:
        node_class = NodeClass()
        node_class.node = node
        node_class.max_score = .0
        node_class.avg_score = .0
        for instance in instances:
            instance_name = instance[0]
            if orphan_word.lower() != instance_name.lower():
                instance_occurrence = instance[1]
                combination = '%s, %s' % (orphan_word, instance_name)
                node_instance = node_class.instances.add()
                node_instance.instance = instance_name
                node_instance.occurrence = instance_occurrence
                node_instance.combination = combination
                node_instance.score = .0
                try:
                    instance[2].append(instance[0])
                    labels = list(map(lambda x: x.replace('_', ' ').lower(), instance[2]))
                    labels = list(set(labels))
                    labels.pop(labels.index(instance[0].replace('_', ' ').lower()))
                    for label in labels:
                        node_instance = node_class.instances.add()
                        node_instance.instance = label
                        node_instance.combination = '%s, %s' % (orphan_word, label)
                        node_instance.score = .0
                except Exception as e:
                    logger.error(e)
        ontology_mapping.append(node_class)
    return ontology_mapping


def get_ontology_mapping_with_instance():
    neo4j = Neo4JBase()
    ontology_mapping = list()
    data = neo4j.query(
        "MATCH (n)-[r:ParentOf*1..1]->(m) \
        WHERE n.Type ='OCLASS' AND m.Type='OINSTANCE' AND not (m.occurrence=null and m.occurrence=0) \
        WITH n, m ORDER BY m.occurrence DESC \
        RETURN n.name, COLLECT([m.name, m.occurrence, labels(m)])[..4] as instances order by instances[0][1] desc")
    nodes = data.values()
    for node, instances in nodes:
        node_class = NodeClass()
        node_class.node = node
        node_class.max_score = .0
        node_class.avg_score = .0
        last_instance = instances.pop()
        last_instance[2].append(last_instance[0])
        last_instance_labels = list(set(last_instance[2]))
        for instance in instances:
            instance_name = instance[0]
            for orphan_word in last_instance_labels:
                if orphan_word.lower() != instance_name.lower():
                    instance_occurrence = instance[1]
                    combination = '%s, %s' % (orphan_word, instance_name)
                    node_instance = node_class.instances.add()
                    node_instance.instance = instance_name
                    node_instance.occurrence = instance_occurrence
                    node_instance.combination = combination
                    node_instance.score = .0
                    try:
                        instance[2].append(instance[0])
                        labels = list(map(lambda x: x.replace('_', ' ').lower(), instance[2]))
                        labels = list(set(labels))
                        labels.pop(labels.index(instance[0].replace('_', ' ').lower()))
                        for label in labels:
                            node_instance = node_class.instances.add()
                            node_instance.instance = label
                            node_instance.combination = '%s, %s' % (orphan_word, label)
                            node_instance.score = .0
                    except Exception as e:
                        logger.error(e)
        ontology_mapping.append(node_class)
    return ontology_mapping


def set_score(topic_id, node_class):
    # Invoking paradigamatic-analyzer lambda in async mode
    try:
        # boto3.setup_default_session(profile_name='rezoomex')
        client = boto3.client('lambda')
        client.invoke(FunctionName='paradigmatic-analyzer-dev', InvocationType='Event', Payload=json.dumps({'topic_id': topic_id, 'node': MessageToJson(node_class)}).encode('utf-8'))
    except Exception as e:
        logger.error(e)


def calculate_paradigmatic(topic_id, ontology_mapping):
    with ThreadPoolExecutor(max_workers=1000) as pool:
        pool.map(set_score, repeat(topic_id), ontology_mapping)


def get_computed_nodes(topic_id, original_nodes_length):
    consumer.subscribe([topic_id])
    computed_nodes = list()
    node_class = NodeClass()
    while True:
        message = consumer.poll(1)
        if message:
            node_class.ParseFromString(message.value())
            node_data = json.loads(MessageToJson(node_class))
            if node_data not in computed_nodes:
                computed_nodes.append(node_data)
            logger.info('computed nodes {}'.format(len(computed_nodes)))
            if len(computed_nodes) == original_nodes_length:
                break;
    max_scored_computed_nodes = sorted(computed_nodes, key=itemgetter('maxScore'), reverse=True)
    # avg_scored_computed_nodes = sorted(computed_nodes, key=itemgetter('avgScore'), reverse=True)
    return max_scored_computed_nodes #, avg_scored_computed_nodes
