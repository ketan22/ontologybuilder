# Paradigmatic Analyzer
## Overview
This module is designed to calculate the paradigmatic similarity score between two words. It is an independent microservice deployed as a lambda function.

It has integration with Elasticsearch to fetch the text data based on given words.

We invoke this lambda function from orphan analyzer to either in synchronous or asynchronous mode. As its wrapped in AWS Lambda function, it can auto scale to serve million of requests asynchronously.

Thats where we are using Apache Kafka to collect the generated result set with a pub/sub model.

Orphan Analyzer is responsible to invoke this lambda function asynchronously and create Kafak consumer with a topic and this lambda produces the result set on the same topic in order to collect the result set dynamically.


## Setup
In order to start working on this module. Its required to enable the virtual environment and install all dependencies packages.

Developers need to follow below mentioned steps to enable to development environment.

1. `cd paradigmatic_analyzer/`
2. Enable the virtual environment using `pipenv shell` command.
3. Install all dependencies using `pipenv install` command. This command should run once only if developer is trying to setup the environment for the first time.
4. Now everything is setup, You can test lambda_handler function by executing the command `python analyzer.py`. It would executes the lambda_handler in local environment. Where you can debug and test the functionality.

## Deployment
We configure this microservice with [Zappa](https://github.com/Miserlou/Zappa). Zappa makes it super easy to build and deploy server-less, event-driven Python applications (including, but not limited to, WSGI web apps) on AWS Lambda + API Gateway. Think of it as "serverless" web hosting for your Python apps. That means infinite scaling, zero downtime, zero maintenance - and at a fraction of the cost of your current deployments!

[Zappa Documentation](https://github.com/Miserlou/Zappa)

Once you configured the zappa and its prerequisites then you can easily deploy or update the deployment as mentioned in below commands.

- For newly/first deployment, run `zappa deploy dev` command. It would executes the new deployment based on zappa_settings.json file.
- For updating existing deployment, run `zappa update dev` command. It would update the existing deployment.


