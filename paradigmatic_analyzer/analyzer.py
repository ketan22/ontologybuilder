import os
import logging
import json
import time
import re
import confluent_kafka

from es import scroll
from numba import jit
from google.protobuf import json_format

from node_pb2 import NodeClass

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


class Paradigmatic:

    def __init__(self, words=None):
        self.data = []
        self.word_phrases = []
        if words:
            for word in words:
                self.word_phrases.append(
                    {"match_phrase": {
                        "text": word
                    }})
            self._set_data_by_word_phrases(self.word_phrases)

    def _get_word_grams(self, text):
        text = text.replace('_', ' ')
        text = text.strip()
        text = text.lower()
        text = text.split(' ')
        return tuple(text)

    def _generate_ngrams(self, text, n):
        return list(zip(*[text.split()[i:] for i in range(n)]))

    def _set_data(self, query_body):
        self.data = []
        for total_pages, page_counter, page_items, page_data in scroll(index=os.environ.get('CORPUS_NAME'), doc_type=os.environ.get('DOC_TYPE'), query_body=query_body):
            logger.info('total_pages={}, page_counter={}, page_items={}'.format(total_pages, page_counter, page_items))
            for data in page_data['hits']['hits']:
                self.data.extend([line.replace('<em>', '').replace('</em>', '') for line in data['highlight']['text']])

    def _set_data_by_word_phrases(self, word_phrases):
        body = {
            "query": {
                "bool": {
                "should": word_phrases
                }
            },
            "_source": "{}",
            "highlight": {
                "fields": {
                "text": {
                    "fragment_size": 200,
                    "number_of_fragments": 0
                    }
                }
            }
        }
        self._set_data(body)


    def _set_data_by_words(self, word1, word2):
        word1 = word1.replace('_', ' ')
        word2 = word2.replace('_', ' ')
        query_body = {
            "query": {
                "bool": {
                "should": [
                    {"match_phrase": {
                    "text": word1
                    }},
                    {"match_phrase": {
                    "text": word2
                    }}
                ]
                }
            },
            "_source": "{}",
            "highlight": {
                "fields": {
                "text": {
                    "fragment_size": 200,
                    "number_of_fragments": 0
                    }
                }
            }
        }
        self._set_data(query_body)


    def get_word_count(self, node_name):
        # Count word occurrences
        node_name = node_name.replace('_', ' ')
        node_name = node_name.replace('-', ' ')
        node_name = node_name.lower()
        node_name = node_name.strip()
        node_name = re.sub('[^a-zA-Z0-9\ ]+', '', node_name)
        return sum([sum(1 for match in re.finditer(r"\b%s\b" % node_name, line)) for line in self.data])


    def _get_left_right_words(self, word):
        assert isinstance(word, tuple), 'Invalid word'
        left_words = []
        right_words = []
        for sentence in self.data:
            sentence = sentence.lower()
            sentence = self._generate_ngrams(sentence, len(word))
            if word in sentence:
                left_words.extend([word_pair[0] for word_pair in sentence[sentence.index(word)-3:sentence.index(word)]])
                right_words.extend([word_pair[0] for word_pair in sentence[sentence.index(word)+1:sentence.index(word)+4]])
        return set(left_words), set(right_words)


    @staticmethod
    def _jaccard_coefficient(a, b):
        try:
            intersectionSize = len(a.intersection(b))
            unionSize = len(a.union(b))
            return float(intersectionSize) / float(unionSize)
        except Exception as e:
            return 0

    @jit
    def similarity(self, w1, w2):
        if not self.word_phrases:
            self._set_data_by_words(w1, w2)
        w1 = self._get_word_grams(w1)
        w2 = self._get_word_grams(w2)
        w1_left_words, w1_right_words = self._get_left_right_words(w1)
        w2_left_words, w2_right_words = self._get_left_right_words(w2)
        data = (self._jaccard_coefficient(w1_left_words, w2_left_words) +
                self._jaccard_coefficient(w1_right_words, w2_right_words)) / 2.0
        return data


def lambda_handler(event, context):

    if 'path' in event and  event['path'] == 'paradigmatic':
        paradigmatic = Paradigmatic()
        score = paradigmatic.similarity(event['word1'], event['word2'])
        occurrence = paradigmatic.get_word_count(event['word2'])
        return {'score': score, 'occurrence': occurrence }

    if 'topic_id' in event:
        conf = {'bootstrap.servers': '13.232.16.189:9092, 13.232.41.199:9092'}
        producer = confluent_kafka.Producer(**conf)
        node_class = json_format.Parse(event['node'], NodeClass(), ignore_unknown_fields=False)
        # word_phrases = [i.instance for i in node_class.instances]
        paradigmatic = Paradigmatic()
        for instance in node_class.instances:
            word1 = instance.combination.split(',')[0]
            word1 = word1.replace('_', ' ')
            word2 = instance.combination.split(',')[1]
            word2 = word2.replace('_', ' ')
            score = paradigmatic.similarity(word1, word2)
            instance.score = score
            instance.occurrence = paradigmatic.get_word_count(instance.instance)
        scores = [i.score for i in node_class.instances]
        node_class.max_score = max(scores) if scores else 0
        node_class.avg_score = sum([ i.occurrence*i.score for i in node_class.instances])/ sum([i.occurrence for i in node_class.instances]) if scores else 0
        messages_to_retry = 0
        topic = event['topic_id']
        msg_payload = node_class.SerializeToString()
        try:
            logger.info('Sending message for {} at {}'.format(node_class.node, topic))
            producer.produce(topic, value=msg_payload)
        except BufferError as e:
            messages_to_retry += 1

        # hacky retry messages that over filled the local buffer
        for i in range(messages_to_retry):
            producer.poll(0)
            try:
                logger.info('Re-Sending message for {} at {}'.format(node_class.node, topic))
                producer.produce(topic, value=msg_payload)
            except BufferError as e:
                producer.poll(0)
                logger.info('Re-Re-Sending message for {} at {}'.format(node_class.node, topic))
                producer.produce(topic, value=msg_payload)
        logger.info('Plushing producer')
        producer.flush()
        logger.info('Plushing completed')



if __name__ == '__main__':
    try:
        # msg_count = 10
        # msg_size = 100
        # from google.protobuf import json_format
        event = {'topic_id': 'blablabla', 'node': json.dumps({'node': 'Messaging', 'instances': [{'instance': 'WCF', 'occurrence': 4494, 'combination': 'kafka, WCF', 'score': 0.0}, {'instance': 'wcf services', 'combination': 'kafka, wcf services', 'score': 0.0}, {'instance': 'w.c.f', 'combination': 'kafka, w.c.f', 'score': 0.0}, {'instance': 'JMS', 'occurrence': 1877, 'combination': 'kafka, JMS', 'score': 0.0}, {'instance': 'Rabbit_Mq', 'occurrence': 1665, 'combination': 'kafka, Rabbit_Mq', 'score': 0.0}, {'instance': 'mq', 'combination': 'kafka, mq', 'score': 0.0}, {'instance': 'rabbitmq', 'combination': 'kafka, rabbitmq', 'score': 0.0}, {'instance': 'rabbit-mq', 'combination': 'kafka, rabbit-mq', 'score': 0.0}], 'maxScore': 0.0, 'avgScore': 0.0})}
        # logger.info(event)
        lambda_handler(event, {})
        # node_class = NodeClass()
        # node_class.node = event['node']['node']
        # for instance in event['node']['instances']:
        #     node_instance = node_class.instances.add()
        #     node_instance.instance = instance['instance']
        #     node_instance.occurrence = instance.get('occurrence', 0)
        #     node_instance.combination = instance['combination']
        #     node_instance.score = instance['score']
        # node_class.max_score = event['node']['max_score']
        # node_class.avg_score = event['node']['avg_score']


        # topic = 'd7399695-9ac5-4c49-970c-79d31ecf5a21'

        # # # conf = {'bootstrap.servers': '13.232.16.189:9092, 13.232.41.199:9092, 35.154.179.0:9092'}
        # # # producer = confluent_kafka.Producer(**conf)
        # messages_to_retry = 0

        # producer_start = time.time()
        # msg_payload = node_class.SerializeToString()
        # for i in range(msg_count):
        #     try:
        #         producer.produce(topic, value=msg_payload)
        #     except BufferError as e:
        #         logger.error(e)
        #         messages_to_retry += 1

        # # hacky retry messages that over filled the local buffer
        # for i in range(messages_to_retry):
        #     producer.poll(0)
        #     try:
        #         producer.produce(topic, value=msg_payload)
        #     except BufferError as e:
        #         logger.error(e)
        #         producer.poll(0)
        #         producer.produce(topic, value=msg_payload)

        # producer.flush()

        # paradigmatic = Paradigmatic()
        # while True:
        #     print ('\nCalculate p aradigmatic similarity ratio.')
        #     word1 = input('Enter word one: ')
        #     word2 = input('Enter word two: ')
        #     if word1 == '' or word1 == '':
        #         print ('Thanks for your time...')
        #         exit(1)
        #     import profile
        #     profile.run('paradigmatic.similarity(word1, word2)')
        #     print ('\nParadigmatic similarity ratio: {0}'.format(paradigmatic.similarity('angular', 'jquery')))
    except Exception as e:
        print (e)
