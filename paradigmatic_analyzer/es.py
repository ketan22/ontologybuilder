import os
import math

from elasticsearch import Elasticsearch
import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


es = Elasticsearch(hosts=os.environ.get('ELASTICSEARCH_HOST'), timeout=300, max_retries=10, retry_on_timeout=True, maxsize=10000)


def scroll(index, doc_type, query_body, page_size=1000, debug=False, scroll='1m'):
    page = es.search(index=index, doc_type=doc_type, scroll=scroll, size=page_size, body=query_body)
    sid = page['_scroll_id']
    sid_list = []
    scroll_size = page['hits']['total']
    total_pages = math.ceil(scroll_size/page_size)
    page_counter = 0
    if debug:
        logger.info('Total items : {}'.format(scroll_size))
        logger.info('Total pages : {}'.format( math.ceil(scroll_size/page_size) ) )
    # Start scrolling
    while (scroll_size > 0):
        # Get the number of results that we returned in the last scroll
        scroll_size = len(page['hits']['hits'])
        if scroll_size>0:
            if debug:
                logger.info('> Scrolling page {} : {} items'.format(page_counter, scroll_size))
            yield total_pages, page_counter, scroll_size, page
        # get next page
        page = es.scroll(scroll_id = sid, scroll = '1m')
        page_counter += 1
        # Update the scroll ID
        sid = page['_scroll_id']
        sid_list.append(sid)
    else:
        if sid_list:
            logger.info('Clearing scroll ids...')
            es.clear_scroll(body={'scroll_id': sid_list})


if __name__ == '__main__':
    index = os.environ.get('CORPUS_NAME')
    doc_type = os.environ.get('DOC_TYPE')
    query = {
                "query": {
                    "match_phrase": {
                            "text": 'java'
                        }
                },
                "_source": "{}",
                "highlight": {
                    "fields": {
                    "text": {
                        "fragment_size": 200,
                        "number_of_fragments": 0
                    }
                    }
                }
            }
    page_size = 1000

    for total_pages, page_counter, page_items, page_data in scroll(index, doc_type, query, page_size=page_size):
        logger.info('total_pages={}, page_counter={}, page_items={}'.format(total_pages, page_counter, page_items))