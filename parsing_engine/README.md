# Parsing Engine
## Overview
This module is designed to parse the document uploaded on AWS S3 bucket and put the parsed text into Elasticsearch document. It is an independent microservice deployed as a lambda function but it is configured to invoke on AWS S3 bucker events.

Hence this lambda function will invoke whenever any file uploaded on AWS S3 bucket.

This lambda functions gets invoke in asynchronous mode for each file uploaded on AWS S3 bucket via S3_uploader module.


## Setup
In order to start working on this module. Its required to enable the virtual environment and install all dependencies packages.

You can test and debug the lambda_handler function by provide a dir of supported documents.

Developers need to follow below mentioned steps to enable to development environment.

1. `cd parsing_engine/`
2. Enable the virtual environment using `pipenv shell` command.
3. Install all dependencies using `pipenv install` command. This command should run once only if developer is trying to setup the environment for the first time.
4. Now everything is setup, You can test lambda_handler function by executing the command `python parsing_engine.py <document_dir>`. It would executes the lambda_handler in local environment and parse text data from all documents and put into Elasticsearch.

## Deployment
We configure this microservice with [Zappa](https://github.com/Miserlou/Zappa). Zappa makes it super easy to build and deploy server-less, event-driven Python applications (including, but not limited to, WSGI web apps) on AWS Lambda + API Gateway. Think of it as "serverless" web hosting for your Python apps. That means infinite scaling, zero downtime, zero maintenance - and at a fraction of the cost of your current deployments!

[Zappa Documentation](https://github.com/Miserlou/Zappa)

Once you configured the zappa and its prerequisites then you can easily deploy or update the deployment as mentioned in below commands.

- For newly/first deployment, run `zappa deploy dev` command. It would executes the new deployment based on zappa_settings.json file.
- For updating existing deployment, run `zappa update dev` command. It would update the existing deployment.




