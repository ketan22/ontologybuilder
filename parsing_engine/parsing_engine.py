import argparse
import os
import re
import docx2txt
import PyPDF2
import html2text
import string
import boto3
import time

from concurrent.futures import ThreadPoolExecutor
from itertools import repeat
from requests.utils import unquote
from datetime import datetime
from elasticsearch import Elasticsearch

import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)


SUPPORTED_FILE_EXTENSIONS = ['doc', 'docx', 'pdf', 'txt', 'html']
es = Elasticsearch(hosts=os.environ.get('ELASTICSEARCH_HOST'), timeout=300, max_retries=10, retry_on_timeout=True, maxsize=50)

class TextReader(object):

    def __init__(self, file, *args, **kwargs):
        self.file = file
        self.filename = file.split('/')[-1:][0]
        self.file_extension = self.filename.split('.')[-1:][0]

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        return self

    def _get_stopwords(self):
        stopwords = ''
        with open('stopwords.txt', 'r') as f:
            stopwords = f.read()
        stopwords = stopwords.split('\n')
        stopwords = list(filter(None, stopwords))
        return stopwords

    def _doc_to_text(self):
        try:
            data = ''
            abs_path, filename = os.path.split(self.file)
            filepath = os.path.join(abs_path, re.escape(filename))
            with os.popen('usr/bin/catdoc {0}'.format(filepath), 'r') as proc:
                data = proc.read()
                data = data.replace('\t', '')
                data = data.split('\n')
                data = list(filter(None, data))
            return data
        except Exception as e:
            pass


    def _docx_to_text(self):
        try:
            data = docx2txt.process(self.file)
            data = data.replace('\t', '')
            data = data.split('\n')
            data = list(filter(None, data))
            return data
        except Exception as e:
            pass


    def _pdf_to_text(self):
        try:
            data = []
            with open(self.file, 'rb') as pdf_file:
                read_pdf = PyPDF2.PdfFileReader(pdf_file)
                number_of_pages = read_pdf.getNumPages()
                for page_number in range(0, number_of_pages):
                    page = read_pdf.getPage(page_number)
                    page_content = page.extractText()
                    page_content.encode('utf-8')
                    page_content = page_content.replace('\t', '')
                    page_content = page_content.split('\n')
                    page_content = list(filter(None, page_content))
                    data.extend(page_content)
            return data
        except Exception as e:
            pass

    def _html_to_text(self):
        try:
            html_parser = html2text.HTML2Text()
            html_parser.single_line_break = True
            html_parser.skip_internal_links = True
            html_parser.ignore_emphasis = True
            html_parser.ignore_images = True
            with open(self.file) as f:
                page_content = html_parser.handle(f.read())
                page_content.encode('utf-8')
                page_content = page_content.replace('\t', '')
                page_content = re.sub(r'\n\n*', '\n', page_content, flags=re.MULTILINE)
                page_content = re.sub('[^a-zA-Z0-9\$\:\-\_\.\+\!\*\'\(\)\,\,\/\=\?\\n ]+?', '', page_content)
                page_content = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%|\-|\-\n|\@|\(|\))*\b', '', page_content, flags=re.MULTILINE)
                page_content = page_content.split('\n')
                page_content = [re.sub('[^a-zA-Z0-9\ ]+', '', _).strip() for _ in page_content]
                page_content = list(filter(None, page_content))
            return page_content
        except expression as identifier:
            pass

    def readlines(self):
        try:
            assert self.file_extension in SUPPORTED_FILE_EXTENSIONS,\
            'Invalid file. Supported file formats are {}'.format(SUPPORTED_FILE_EXTENSIONS)
            if self.file_extension == 'docx':
                return self._docx_to_text()
            elif self.file_extension == 'pdf':
                return self._pdf_to_text()
            elif self.file_extension == 'doc':
                return self._doc_to_text()
            elif self.file_extension == 'html':
                return self._html_to_text()
            logger.info(self.file_extension)
        except Exception as e:
            pass
        return []

    def parsed_text(self):
        try:
            data = []
            regex = re.compile('[%s]' % re.escape(string.punctuation))
            exclude = set(string.punctuation)
            for sentence in self.readlines():
                sentence = sentence.lower()
                sentence = regex.sub('', sentence)
                sentence = re.sub('[^A-Za-z0-9]+', ' ', sentence)
                sentence = sentence.strip()
                if sentence and len(sentence) > 1:
                    sentence = ' '.join([i for i in sentence.split(' ') if i not in self._get_stopwords()])
                    data.append(sentence)
            return data
        except Exception as e:
            logger.info(e)


def lambda_handler(event, context):
    s3 = boto3.resource('s3')
    logger.info(event['Records'])
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']
        key = unquote(key).replace('+', ' ')
        filepath = os.path.join('/tmp', key)
        logger.info("****************************************************************")
        logger.info(bucket)
        if not es.exists(index=bucket, doc_type='docs', id=key):
            logger.info(key)
            logger.info(filepath)
            s3.Bucket(bucket).download_file(key, filepath)
            with TextReader(filepath) as file:
                logger.info("Here 1")
                logger.info(file.parsed_text())
                doc = {
                        'text': file.parsed_text(),
                        'timestamp': datetime.now()
                }
                logger.info("Here 2")
                logger.info(doc)
                res = es.index(index=bucket, doc_type='docs', id=file.filename, body=doc)
                print(res)
                print(dir(res))
                es.indices.refresh(index=bucket)
                logger.info('File uploaded successfully.')
        else:
            logger.info("File %s already exists !!" % key)
        print("****************************************************************")

def upload(path, bucket_name, filepath):
    try:
        logger.info('processing...')
        with TextReader(os.path.join(path, filepath)) as file:
            if not es.exists(index=bucket_name, doc_type='docs', id=file.filename):
                doc = {
                        'text': file.parsed_text(),
                        'timestamp': datetime.now()
                }
                res = es.index(index=bucket_name, doc_type='docs', id=file.filename, body=doc)
                es.indices.refresh(index=bucket)

                logger.info('%s uploaded successfully.' % file.filename)
            else:
                logger.info("File %s already exists !!" % key)
    except Exception as e:
        logger.info(e)
        logger.info('Failed %s' % filepath)

def local_handler(data_src):
    t0 = time.time()
    bucket_name = os.environ.get('CORPUS_NAME')

    with ThreadPoolExecutor(50) as pool:
        for path, dirs, files in os.walk(os.path.abspath(data_src)):
            pool.map(upload, repeat(path), repeat(bucket_name), files)
    print('Time elapsed:', (time.time() - t0), 'seconds')


# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(description='Provide a corpus path.')
#     parser.add_argument('corpus', metavar='Corpus path', type=str,
#                         help='Directory path of your corpus')
#     args = parser.parse_args()
#     local_handler(args.corpus)