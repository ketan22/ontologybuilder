# S3 Uploader
## Overview
This module is designed to upload documents files into AWS S3 bucket. Its built using Python's micro framework named as [Flask](http://flask.pocoo.org/) and [Flask Restful](https://flask-restful.readthedocs.io/en/latest/).
There is a single endpoint as mentioned below.

*Note: Once the files are uploaded on AWS S3 then S3 would trigger the parsing engine because parsing engine is configured the AWS S3 bucket events. Hence all files will get parsed by parsing engine and the text corpus would be updated.*

### `/files`
This endpoint is intended to accept multiple files to upload.

Example:
![File upload example](https://s3.ap-south-1.amazonaws.com/orphan-allocation/Screenshot+from+2018-05-10+01-27-01.png)

## Setup
In order to start working on this module. Its required to enable the virtual environment and install all dependencies packages.

Developers need to follow below mentioned steps to enable to development environment.

1. `cd orphan_analyzer/`
2. Enable the virtual environment using `pipenv shell` command.
3. Install all dependencies using `pipenv install` command. This command should run once only if developer is trying to setup the environment for the first time.
4. Now everything is setup, You can server the endpoint resources by executing the command `python resources.py`. It would start the localhost server at port 5000.

*Note:
If you want to upload any directory of bulk documents then you can comment out this line `app.run(debug=True)` in resources.py file and executes `python resources.py <dir_path>` It would automatically upload all files from the given directory.*

## Deployment
We configure this microservice with [Zappa](https://github.com/Miserlou/Zappa). Zappa makes it super easy to build and deploy server-less, event-driven Python applications (including, but not limited to, WSGI web apps) on AWS Lambda + API Gateway. Think of it as "serverless" web hosting for your Python apps. That means infinite scaling, zero downtime, zero maintenance - and at a fraction of the cost of your current deployments!

[Zappa Documentation](https://github.com/Miserlou/Zappa)

Once you configured the zappa and its prerequisites then you can easily deploy or update the deployment as mentioned in below commands.

- For newly/first deployment, run `zappa deploy dev` command. It would executes the new deployment based on zappa_settings.json file.
- For updating existing deployment, run `zappa update dev` command. It would update the existing deployment.
