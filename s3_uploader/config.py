

class Config(object):
    DEBUG = False
    ALLOWED_EXTENSIONS = ['doc', 'docx', 'pdf', 'txt', 'html']
    FILE_CONTENT_TYPES = {
        # these will be used to set the content type of S3 object. It is binary by default.
        'doc': 'application/msword',
        'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'pdf': 'application/pdf',
        'txt': 'text/plain',
        'html': 'text/html'
    }

class ProductionConfig(Config):
    DEBUG = False

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True

config = {
    'dev': DevelopmentConfig,
    'prod': ProductionConfig,
    'test': TestingConfig
}