import os
import boto3
import argparse
from concurrent.futures import ThreadPoolExecutor
from itertools import repeat

from flask import Flask
from flask_restful import Api as FlaskRestfulAPI, Resource, reqparse, abort
from werkzeug.datastructures import FileStorage

from config import config
import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)
# boto3.setup_default_session(profile_name='default')


def upload_s3(bucket_name, file):
    s3 = boto3.client('s3')
    filename = file.filename if hasattr(file, 'filename') else file.name.split('/')[-1:][0]
    try:
        s3.create_bucket(
            Bucket=bucket_name,
            CreateBucketConfiguration={'LocationConstraint': os.environ['AWS_DEFAULT_REGION']}
        )
    except Exception as e:
        pass
    s3.upload_fileobj(file, bucket_name, filename)
    logger.info('File uploaded {}'.format(filename))

class UploadFileResource(Resource):
    put_parser = reqparse.RequestParser()
    put_parser.add_argument('files', required=True, type=FileStorage, location='files', action='append')
    put_parser.add_argument('corpus_name', required=True)
    def put(self):
        #TODO: a check on file size needs to be there.
        
        args = self.put_parser.parse_args()
        files = args['files']

        # check file extension
        for file in files:
            extension = file.filename.rsplit('.', 1)[1].lower()
            if '.' in file.filename and not extension in app.config['ALLOWED_EXTENSIONS']:
                abort(400, message="File extension is not one of our supported types.")
            bucket_name = args['corpus_name']
            upload_s3(bucket_name, file)

        return {'message': 'files uploaded successfully'}


def upload(path, bucket_name, filepath):
    with open(os.path.join(path, filepath)) as file:
        upload_s3(bucket_name, file)

# Get the details(path) of files and upload it to S3 bucket    
def local_handler(data_src):
    bucket_name = os.environ.get('CORPUS_NAME')
    for path, dirs, files in os.walk(os.path.abspath(data_src)):
        with ThreadPoolExecutor() as pool:
            pool.map(upload, repeat(path), repeat(bucket_name), files)

app = Flask(__name__)
app.config.from_object(config['prod'])
api = FlaskRestfulAPI(app)
api.add_resource(UploadFileResource, '/files')

# if __name__ == '__main__':
#     app.run(debug=True)
    # parser = argparse.ArgumentParser(description='Provide a corpus path.')
    # print("-----------------------------")
    # print(parser)
    # parser.add_argument('corpus', metavar='Corpus path', type=str, help='Directory path of your corpus')
    # args = parser.parse_args()
    # local_handler(args.corpus)